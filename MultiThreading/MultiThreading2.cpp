// MultiThreading example (5 threads increase file.txt context concurrently.
// Uses <pthread.h> lib and uses mutex feature.

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <pthread.h>

using namespace std;

#define NUM_THREADS	5

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;


int *findsum(int a, int b);


void *PrintHello(void *threadid)
{
	pthread_mutex_lock( &mutex1 );
	ifstream fin("file.txt");

	long tid = (long)threadid;
	cout << "Hello World! Thread ID, " << tid << endl;
	int x;
	fin >> x;
	fin.close();

	for (int k=0;k<2000000000;k++) { };
	ofstream fout("file.txt");
	fout << x+1;
	fout.close();
	pthread_mutex_unlock( &mutex1 );

	cout << "Thread ID, " << tid << " is finished" << endl;
	pthread_exit(NULL);
}

int main ()
{
	pthread_t threads[NUM_THREADS];
	int rc;
	int i;

	for( i=0; i < NUM_THREADS; i++ )
	{
		cout << "main() : creating thread, " << i << endl;
		rc = pthread_create(&threads[i], NULL, PrintHello, (void *)i); // pthread_create returns zero if success, otherwise returns an error code.
//		cout << "rc: " << rc << endl;
		if (rc)
		{
			cout << "Error:unable to create thread," << rc << endl;
			exit(-1);
		}
	}

	pthread_exit(NULL);

	int x = 4, y = 2;

	cout << findsum(x,y) << endl;
}


int *findsum(int a, int b)
{
	int * c;
	*c = a+b;
	return c;
}
