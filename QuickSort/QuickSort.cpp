// QuickSort
// Best: n.logn
// Avg: h.logn
// Worst: n ^ 2

#include <iostream>
using namespace std;

void quicksort(int arr[], int start, int end);

int main()
{
	int my_arr[] = {5,3,6,1,4,7,2,8};
	int size = sizeof(my_arr)/sizeof(int);

	quicksort(my_arr, 0, 7);

	cout << "Sorted array:\n";
	for(int i=0;i<size;i++)
	{
		cout << my_arr[i] << " ";
	}
	cout << endl;
}

void quicksort(int arr[], int start, int end)
{
	int left = start, right = end, pivot = start;
	while(left < right)
	{
		while (left != pivot)
		{
			if (arr[left] > arr[pivot])
			{
				int temp = arr[pivot];
				arr[pivot] = arr[left];
				arr[left] = temp;
				pivot = left;
			}
			else
				left++;
		}


		while (right != pivot)
		{
			if (arr[right] < arr[pivot])
			{
				int temp = arr[pivot];
				arr[pivot] = arr[right];
				arr[right] = temp;
				pivot = right;
			}
			else
				right--;
		}

	}

	if ( start < pivot-1)
		quicksort(arr,start,pivot-1);
	if ( pivot+1 < end )
		quicksort(arr,pivot+1,end);
}
