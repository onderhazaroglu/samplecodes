// TreeAlgorithms
// Breadth-First Search, Depth-First Search
// Runtime Performance Complexity : O (|E|), O( b ^ d ); 	b: branching factor,	d: depth
// Space Complexity : O (|V|), O( b ^ d ); 	b: branching factor,	d: depth

#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <stack>
using namespace std;

struct node;

typedef node * tnode;

struct node
{
	string name;
	vector<tnode> branches;
	tnode parent;
};

bool bfs(tnode root, string key);
bool dfs(tnode root, string key);

int main()
{
	tnode root = new node;
	root->name = "1";
	root->parent = NULL;

	tnode temp = new node;

	temp->name = "2";
	temp->parent = root;
	root->branches.push_back(temp);

	temp = new node;
	temp->name = "3";
	temp->parent = root;
	root->branches.push_back(temp);

	temp = new node;
	temp->name = "4";
	temp->parent = root;
	root->branches.push_back(temp);

	temp = new node;
	temp->name = "5";
	temp->parent = root->branches[0];
	root->branches[0]->branches.push_back(temp);

	temp = new node;
	temp->name = "6";
	temp->parent = root->branches[0];
	root->branches[0]->branches.push_back(temp);

	temp = new node;
	temp->name = "9";
	temp->parent = root->branches[0]->branches[0];
	root->branches[0]->branches[0]->branches.push_back(temp);

	temp = new node;
	temp->name = "10";
	temp->parent = root->branches[0]->branches[1];
	root->branches[0]->branches[1]->branches.push_back(temp);

	temp = new node;
	temp->name = "11";
	temp->parent = root->branches[0]->branches[1];
	root->branches[0]->branches[1]->branches.push_back(temp);

	temp = new node;
	temp->name = "7";
	temp->parent = root->branches[1];
	root->branches[1]->branches.push_back(temp);

	temp = new node;
	temp->name = "8";
	temp->parent = root->branches[2];
	root->branches[2]->branches.push_back(temp);

	temp = new node;
	temp->name = "12";
	temp->parent = root->branches[2]->branches[0];
	root->branches[2]->branches[0]->branches.push_back(temp);

	temp = new node;
	temp->name = "13";
	temp->parent = root->branches[2]->branches[0];
	root->branches[2]->branches[0]->branches.push_back(temp);


	cout << root->branches[0]->branches[1]->name << endl;
	cout << root->branches[1]->name << endl;

	string k = "13";

	if ( bfs(root, k) ) cout << k << " is found in tree.\n";
	else cout << k << " is not found!\n";

	if ( dfs(root, k) ) cout << k << " is found in tree.\n";
	else cout << k << " is not found!\n";

	return 0;
}

bool bfs(tnode root, string key)
{
	stack<tnode> path;

	bool found = false;
	tnode temp;
	queue<tnode> q;
	if ( root != NULL )
		q.push(root);

	while( !q.empty() )
	{
		temp = q.front();
		q.pop();

		if ( temp->name == key )
		{
			found = true;
			break;
		}
		else
			for(int i=0;i< temp->branches.size();i++) q.push(temp->branches[i]);
	}

	if ( found )
	{
		tnode par = temp;
		while( par != NULL )
		{
			path.push(par);
			par = par->parent;
		}
		while( !path.empty() )
		{
			cout << path.top()->name;
			path.pop();
			if ( !path.empty() ) cout << "->";
		}
		cout << endl;
	}
	return found;
}

bool dfs(tnode root, string key)
{
	stack<tnode> path;

	bool found = false;
	tnode ptr = root, temp;
	stack<tnode> s;
	if ( ptr != NULL )
		s.push(ptr);

	while( !s.empty() )
	{
		temp = s.top();
		s.pop();
		if ( temp->name == key )
		{
			found = true;
			break;
		}
		else
			for(int i=0;i< temp->branches.size();i++) s.push(temp->branches[i]);
	}

	if ( found )
	{
		tnode par = temp;
		while( par != NULL )
		{
			path.push(par);
			par = par->parent;
		}
		while( !path.empty() )
		{
			cout << path.top()->name;
			path.pop();
			if ( !path.empty() ) cout << "->";
		}
		cout << endl;
	}
	return found;
}
