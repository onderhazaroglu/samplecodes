# Echo server program
import socket, sys

HOST = ''                 # Symbolic name meaning the local host
PORT = 12000              # Arbitrary non-privileged port
try:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind((HOST, PORT))
	s.listen(1)
	print 'Server is ready'
except socket.error, (value, message):
	if s:
		s.close()
	print 'Could not open socket: '+message
	sys.exit(1)

while 1:
	conn, addr = s.accept()
	print 'Connected by', addr
	data = conn.recv(1024)
	print 'Data: '+data
	if not data: break
	conn.send(data)
conn.close()
