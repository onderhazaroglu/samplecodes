#include <iostream>
#include <string>
using namespace std;

template<class T1, class T2>
class HashTable
{
	private:
		const int size;
		T1 * first;
		T2 * second;

	public:
		HashTable(): size(128), first(new T1[size]), second(new T2[size]) {}

		HashTable(int nsize): size(nsize) {
			// size = nsize;
			first = new T1[size];
			second = new T2[size];
		}

		~HashTable() {
			delete[] first;
			delete[] second;
			cout << "object deleted." << endl;
		}


		// Getters
		int get_size() { return size; }

		T1 get_first(int index) { return first[index]; }
		T2 get_second(int index) { return second[index]; }

		// Setters
		void set_first(int index, T1 val) { first[index] = val; }
		void set_second(int index, T2 val) { second[index] = val; }


		int hash_function(T1 key) {
			int index = 0, k = 0;

			do {
				index += int( key[k++] ) % size;
			}
			while (first[index] != NULL && key != first[index] );

			return index ;
		}


		void insert(T1 fval, T2 sval) {
			int index = hash_function(fval);
			this->first[index] = fval;
			this->second[index] = sval;
		}


		T2 get_value(T1 fval) {
			int index = hash_function( fval );
			return this->second[index];
		}
};


int main() {

	/*
	cout << "A" << endl;
	char * mystr = (char *)"12345";
	cout << "B" << endl;
	char buffer [50];
	cout << "C" << endl;
	*/

	// string a = "12345";

	/*
	sprintf( buffer, "%s", mystr);
	cout << "D" << endl;

	cout << buffer << endl;
	cout << "E" << endl;
	*/

	HashTable<char *, char *> myHash;
	HashTable<char *, int> myHash2(64);

	char * str = (char *)"deneme";

	myHash.set_first(5, str);

	myHash.insert( (char *)"onder", (char *)"11111");
	cout << myHash.get_value( (char *)"onder" ) << endl;

	/*
	HashTable<int, int> myHash3;
	myHash3.insert( 1111, 45 );
	cout << myHash3.get_value( 1111 ) << endl;
	*/

	return 0;
}
