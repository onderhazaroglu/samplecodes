#include <iostream>
using namespace std;

struct cell
{
	int num;
	cell* next;
};

typedef cell* cellPtr;

void bubblesort(cellPtr head, int size);
void printlist(cellPtr head, int count);

int main()
{
	int element = 0, count = 0;
	cellPtr head = new cell, tail;
	tail = head;

	while(element != -1) {
		cout << "Please enter an element(-1 to quit): ";
		cin >> element;

		if (element > -1) {
			if (count == 0) {
				head->num = element;
			}
			else {
				tail->next = new cell;
				tail = tail->next;
				tail->num = element;
			}
			count++;
		}
	}

	printlist(head, count);
	bubblesort(head,count);
	printlist(head, count);

	return 0;
}


void printlist(cellPtr head, int count){
	cellPtr tail = head;

	for(int i=0;i<=count;i++) {
		cout << tail->num;

		if (tail->next != NULL) {
			tail = tail->next;
			cout << " -> ";
		}
		else break;
	}
	cout << endl;
}


void bubblesort(cellPtr head, int size){
	cellPtr p = head;
	int i = 1, j, temp, a, c = size;
	bool swapped;

	while(i<size) {
		j = i;
		p = head;
		swapped = false;
		while(j++<size){
			cout << "i: " << i << ", j: " << j << ", size: " << size << endl;
			cout << "Comparing " << p->num << " with " << p->next->num << endl;
			if(p->num > p->next->num) {
				cout << "Swapping..." << endl;
				temp = p->num;
				p->num = p->next->num;
				p->next->num = temp;
				swapped = true;
				printlist(head,c);
			}
			p = p->next;
			// cin >> a;
		}
		if (swapped) size--;
		cin >> a;
	}
}
