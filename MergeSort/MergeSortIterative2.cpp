#include <iostream>
using namespace std;

void merge(int arr[], int start, int middle, int end, int asize);
void merge_sort(int arr[], int first, int last, int asize);

int min(int a, int b)
{
	if (a<=b) return a;
	else return b;
}

int main()
{
	int my_arr[] = {5,3,6,1,4,7,2,8};
	int size = sizeof(my_arr)/sizeof(int);
	merge_sort(my_arr,0,7,size);

	for(int i=0;i<size;i++)
		cout << my_arr[i] << " ";
	cout << endl;
	return 0;
}

void merge(int marr[], int start, int middle, int end, int asize)
{
	int *temp_merge = new int[end-start];
	int l = 0, r = 0, i = 0;
	while( l < middle - start && r < end - middle)
	{
		if (marr[start+l] < marr[middle+r])
			temp_merge[i++] = marr[start+ l++];
		else
			temp_merge[i++] = marr[middle+ r++];
	}

	while( r < end - middle ) temp_merge[i++] = marr[middle + r++];
	while( l < middle - start ) temp_merge[i++] = marr[start + l++];

	for(int k=0;k<end-start;k++)
		marr[start+k] = temp_merge[k];
	delete(temp_merge);
}

void merge_sort(int arr[], int first, int last, int asize)
{
	int arr_len = last-first+1;
	for (int i=1;i<=arr_len/2+1;i *= 2)
		for(int j=i;j<arr_len;j+= 2*i)
			merge(arr, j-i, j, min(j+i,arr_len),asize);
}
