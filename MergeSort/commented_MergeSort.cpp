#include <iostream>
using namespace std;

void merge_sort(int* arr, int first, int last);
void merge(int* arr, int a_first, int a_last, int b_first, int b_last);

int main()
{
	// cout << "Enter a number (-1 to quit) : " << endl;
	int my_arr[] = {5,3,6,1,2,4,8,7};

	// int size = sizeof(my_arr) / sizeof(int);

	// cout << size << endl;

	merge_sort(my_arr, 0, 7);

	cout << "FINAL SORTED ARRAY: \n";
	for (int i=0;i<8;i++)
	{
		cout << my_arr[i] << " ";
	}
	cout << endl;

	return 0;
}


void merge(int* arr, int a_first, int a_last, int b_first, int b_last)
{
	int tempsize = b_last-a_first+1, i = 0;
	int temp[tempsize];
	int a_cur = a_first;
	int b_cur = b_first;

	while(a_cur <= a_last && b_cur <= b_last)
	{
		if (arr[a_cur] < arr[b_cur])
		{
			temp[i] = arr[a_cur];
			a_cur++;
		}
		else
		{
			temp[i] = arr[b_cur];
			b_cur++;
		}
		i++;
	}
	while(a_cur <= a_last)
	{
		temp[i] = arr[a_cur];
		i++;
		a_cur++;
	}
	while(b_cur <= b_last)
	{
		temp[i] = arr[b_cur];
		i++;
		b_cur++;
	}

	cout << "temp: \n";
	for(int u=0;u<tempsize;u++)
	{
		cout << temp[u] << " ";
	}
	cout << endl;

	for(unsigned int j=0;j<sizeof(temp)/sizeof(int);j++)
	{
		arr[a_first+j] = temp[j];
	}
}




void merge_sort(int* arr, int first, int last)
{
	int mid; //, size = last - first + 1;

	for(int o=0;o<=last-first;o++)
	{
		cout << arr[first+o] << " ";
	}
	cout << endl;

	if (last > first)
	{
		mid = (last + first) / 2;
		cout << "mid: " << mid << endl;
		if (mid > first)
		{
			cout << "merge_sort(arr, " << first << ", " << mid << ")" << endl;
			merge_sort(arr, first, mid);
		}

		if (last > mid+1)
		{
			cout << "merge_sort(arr, " << mid+1 << ", " << last << ")" << endl;
			merge_sort(arr, mid+1,last);
		}
		merge(arr, first, mid, mid+1, last);
	}


/*	for(int i=0;i<size;i++)
	{
		cout << temp[i] << " ";
	}
	cout << endl;
*/

//	return arr;
}
