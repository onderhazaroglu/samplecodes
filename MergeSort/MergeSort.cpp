// MergeSort
// Best: n.logn
// Avg: nlogn
// Worst: nlogn

#include <iostream>
using namespace std;

void merge_sort(int* arr, int first, int last);
void merge(int* arr, int a_first, int a_last, int b_first, int b_last);

int main()
{
	int my_arr[] = {5,3,6,1,2,4,8,7};
	merge_sort(my_arr, 0, 7);

	cout << "FINAL SORTED ARRAY: \n";
	for (int i=0;i<8;i++)
		cout << my_arr[i] << " ";
	cout << endl;
	return 0;
}


void merge(int* arr, int a_first, int a_last, int b_first, int b_last)
{
	int tempsize = b_last-a_first+1, i = 0;
	int temp[tempsize];
	int a_cur = a_first;
	int b_cur = b_first;

	while(a_cur <= a_last && b_cur <= b_last)
		if (arr[a_cur] < arr[b_cur])
			temp[i++] = arr[a_cur++];
		else
			temp[i++] = arr[b_cur++];

	while(a_cur <= a_last)
		temp[i++] = arr[a_cur++];

	while(b_cur <= b_last)
		temp[i++] = arr[b_cur++];

	for(unsigned int j=0;j<sizeof(temp)/sizeof(int);j++)
		arr[a_first+j] = temp[j];
}

void merge_sort(int* arr, int first, int last)
{
	int mid;

	if (last > first)
	{
		mid = (last + first) / 2;
		if (mid > first)
			merge_sort(arr, first, mid);
		if (last > mid+1)
			merge_sort(arr, mid+1,last);
		merge(arr, first, mid, mid+1, last);
	}
}
