#include <iostream>
using namespace std;

void merge(int arr[], int start, int middle, int end, int asize);
void merge_sort(int arr[], int first, int last, int asize);

int min(int a, int b)
{
	if (a<=b) return a;
	else return b;
}



int main()
{
	int my_arr[] = {5,3,6,1,4,7,2,8};
	int size = sizeof(my_arr)/sizeof(int);
	cout << "SIZE: " << size << endl;
	merge_sort(my_arr,0,7,size);

	for(int i=0;i<size;i++)
	{
		cout << my_arr[i] << " ";
	}
	cout << endl;

	return 0;
}


void merge(int marr[], int start, int middle, int end, int asize)
{
	cout << "array baslangic: " << asize << endl;
	for(int i=0;i<asize;i++)
	{
		cout << marr[i] << " ";
	}
	cout << endl;

	int *temp_merge = new int[end-start];
	cout << "\tmerge call: start=" << start << ", middle=" << middle << ", end=" << end << endl;
	int l = 0, r = 0, i = 0;
	while( l < middle - start && r < end - middle)
	{
		cout << "ilk eleman: " << marr[start+l] << endl;
		cout << "comparing " << marr[start+l] << " - " << marr[middle+r] << endl;
		if (marr[start+l] < marr[middle+r])
		{
			temp_merge[i++] = marr[start+ l++];
			cout << "sol kucuk\n";
		}
		else
		{
			temp_merge[i++] = marr[middle+ r++];
			cout << "sag kucuk\n";
		}

		cout << "Temp-merge : " << i << " -> ";
		for(int g=0;g<i;g++)
		{
			cout << temp_merge[g] << " ";
		}
		cout << endl;
	}

	while( r < end - middle ) temp_merge[i++] = marr[middle + r++];
	while( l < middle - start ) temp_merge[i++] = marr[start + l++];

	cout << "Temp-merge: " << i << " -> ";
	for(int g=0;g<i;g++)
	{
		cout << temp_merge[g] << " ";
	}
	cout << endl;

	for(int k=0;k<end-start;k++)
	{
		cout << "degistir: marr[start+k" << k << "]=" << marr[start+k] << " from temp_merge[" << k << "]=" << temp_merge[k] << endl;
		marr[start+k] = temp_merge[k];
	}

	cout << "array son: " << asize << endl;;
	for(int i=0;i<asize;i++)
	{
		cout << marr[i] << " ";
	}
	cout << endl;

	delete(temp_merge);
}


void merge_sort(int arr[], int first, int last, int asize)
{
	int arr_len = last-first+1;
	cout << "merge_sorta gelen array size: " << asize << endl;

	for (int i=1;i<=arr_len/2+1;i *= 2)
	{
		for(int j=i;j<arr_len;j+= 2*i)
		{
			cout << "j-i=" << j-i << ", j=" << j << ", j+i=" << j+i << ", arr_len=" << arr_len << endl;
			merge(arr, j-i, j, min(j+i,arr_len),asize);
		}
	}
}
